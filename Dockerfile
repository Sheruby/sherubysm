FROM ubuntu:20.04

# install Python and Python VENV
RUN apt-get update && apt-get install -y python3.9 python3.9-dev && apt-get -y install python3.9-venv

# Copy everything from the project to the docker image
COPY . .

# create and activate virtual environment
RUN python3.9 -m venv /opt/venv
ENV PATH="/opt/venv/bin:$PATH"
COPY . .

RUN pip install --no-cache-dir --upgrade pip && pip install --no-cache-dir -r requirements.txt

# activate virtual environment
ENV VIRTUAL_ENV=/opt/venv
ENV PATH="/opt/venv/bin:$PATH"


# Expose the port 5000
EXPOSE 5000

# Run the Python file
CMD ["python", "main.py"]
